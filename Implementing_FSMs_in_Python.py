'''@File        Implementing_FSMs_in_Python.py
@brief          Simulates a set of elevators
@details        Implement a finite state machine, shiwn below, to simulate 
                the behavior of a set of elevators
@author         Christopher Edward
@date           1/27/21
'''

import time
import random


# Function definitions go here
def motor_cmd(cmd):
    '''@brief       Commands the motor to move or stop
    @param cmd      The command to give the motor
    '''
    if cmd=='FWD':
        print('Elevator down')
    elif cmd=='REV':
        print('Elevator up')
    elif cmd=='STOP':
        print('Elevator stop')

def floor_1():
    '''@brief       Sensors if the elevator reaches first floor
    '''
    return random.choice([True, False]) # randomly return T or F

def floor_2():
    '''@brief       Sensors if the elevator reaches second floor
    '''
    return random.choice([True, False]) # randomly return T or F

def button_1():
    '''@brief       Sensors if first floor button is selected or not
    '''
    return random.choice([True, False]) # randomly return T or F

def button_2():
    '''@brief       Sensors if second floor button is seleced or not
    '''
    return random.choice([True, False]) # randomly return T or F


# Main program / test program begin
# This code only runs if the script is executed as main by pressing play
# but does not run if the script is imported as a module
if __name__ == "__main__":
    #Program initialization goes here
    state = 0 # Initial state is the init state
    
    while True:
        '''@brief       Loop for the Finite state machine
        @details        This is the loop for finite state machine to keep on going
                        until the code is interupted
        '''
        try:
           #main rogram code goes here
           if state==0:
               '''@brief    Conditional statements for the elevator
               @details     This section is the conditional statement for the elevators,
                            depending on which state the elavator is at. Each state tells
                            represent the situation of the elevator and has different commands
               '''
               #run state 1 (init) code
               print('S0')
               motor_cmd('FWD') # Command elevator to go down
               state = 1 # Updating state for next iteration
               
           elif state==1:
               #run state 1 (moving down) code
               print('S1')
               if floor_1(): 
                   motor_cmd('STOP') 
                   button_1() 
                   state = 2 
                   
           elif state==2:
               #run state 2 (stop on first floor) code
               print ('S2')
               if button_2():
                   motor_cmd('REV')
                   state = 3
                   
               else:
                     state = 2
                    
           elif state==3:
                #run state 3 (moving up) code
               print('S3')
               if floor_2():
                   motor_cmd('STOP') 
                   button_2() 
                   state = 4 
                   
           elif state==4:
                #run state 2 (stop on first floor) code
               print ('S4')
               if button_1():
                   motor_cmd('FWD')
                   state = 1
                   
               else:
                   state = 4
                     
           else:
                pass
            # code to run if state number is invalid
            # program should ideally never reach here
            
            # Slow down execution of FSM so we can see output in console    
           time.sleep(0.2)
            
        except KeyboardInterrupt:
            '''@brief       Interuption to stop the code
            '''
            # This except block catches "Ctrl-C" from the keyboard to end the 
            # while(true) loop when desired
            break