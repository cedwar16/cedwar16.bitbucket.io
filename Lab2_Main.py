'''@file        Lab2_Main.py
@brief          Simulates 3 patterns of LED with button command
@details        Simulating three patterns of LED using Nucleo board 
                depending on the external input from the button
@author         Christopher Edward
@date           2/04/21
'''

import utime
import pyb

def ncycles(iterable, n): #Returns the sequence elements n times
    from itertools import chain, repeat
    return chain.from_iterable(repeat(iterable, n))

myVar = 0
pinC13 = pyb.Pin (pyb.Pin.cpu.C13, pyb.Pin.OUT_PP) #create a pin object for PC13

def myCallback(IRQ_source): 
    global myVar
    myVar += 1
    print(myVar)

if __name__ == "__main__":
    
    state = 0
    
    while True:
        try:
            
            if state==0:
                
                print('Welcome to Nucleo! Please press the blue button to switch LED patterns')
                if myCallback(): #transition from state 0 to state 1 after the buttin is pressed
                    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=myCallback)
                state = 1
                
            elif state==1:
                print('This is the Square wave pattern.')
                #setting up the square wave pattern
                tim2 = pyb.Timer(2, freq = 20000)
                t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinC13)

                square = [t2ch1.pulse_width_percent(100), pyb.delay(500), utime.sleep(0.5)]

                for x in ncycles(square, 10): #looping the sequence 10 times
                    print(x)
                    
                if myCallback(): #transition from state 1 to state 2 after the buttin is pressed
                    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=myCallback)
                    state=2
                    
            elif state==2:
                print('This is the sine wave pattern.')
                #setting up the sine wave sequence 
                tim2 = pyb.Timer(2, freq = 20000)
                t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinC13)
                
                for s in range(50,100): #loop for the sine wave
                    print(t2ch1.pulse_width_percent(s))
                
                if myCallback(): #transition from state 2 to state 3 after the buttin is pressed
                    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=myCallback)
                    state=3
                    
            elif state==3:
                print('This is the sawtooth pattern.')
                #Setting up the sawtooth wave sequence
                tim2 = pyb.Timer(2, freq = 20000)
                t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinC13)
                
                
                
                for i in range(0,100): #loop for the sawtooth wave
                    print(t2ch1.pulse_width_percent(i))
                    
                
                
                if myCallback(): #transition from state 3 to state 4 after the buttin is pressed
                    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=myCallback)
                    state=4
                    
            elif state==4: #return to state 1 after the third pattern rolls
                print('Those were the 3 LED patterns, going back to square wave pattern.')
                state=1
                
            else:
                pass
            
        except KeyboardInterrupt:
            print('Thank you for playing, see you soon!')
            break
