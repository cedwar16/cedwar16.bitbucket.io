## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  Some information about the whole project
#
#  @section sec_lab1 Lab 1 Fibonacci Sequence
#  This is fibonacci lab 1. the program will ask the user to types a number of how many number the program wants to generate.
#  and the program will generate the number as a fibonacci sequence. 
#
#  Link: https://bitbucket.org/cedwar16/me305_labs/src/master/lab%201/lab01_fibonacci.py
#
#  @section sec_lab2 Lab 2 Square, Sinusoidal, and Seesaw Blinking Light function
#  This is assignment lab 2. the program will make a light to blink in 3 different patterns:
#  sinusoidal, seesaw, and square wave. the user will have to press a button to switch between 
#  patterns.
#
#  Link: https://bitbucket.org/cedwar16/me305_labs/src/master/Lab%202/Lab2_Main.py
#
#  @section sec_lab3 Lab 3 Simon Says 
#  This is assignment lab 3. The program will play a game of simon says with the user.
#  It will blink some pattern and the user has to press a button according to the pattern.
#  The number of pattern will increase upto 10 and the user wins if they can follow  all the patterns. 
#
#  Link: https://bitbucket.org/cedwar16/me305_labs/src/master/Lab%203/SimonSaysTask.py
#
#  @section sec_FinalProjectWeek1 Final Project Week 1
#  This is week 1 of the final project. We are making a communication between the hardware and the program 
#  so that the program can collect, and generate the data. andd while at it, stop collecting data when 
#  a button is pressed.
#
#  Link: https://bitbucket.org/cedwar16/me305_labs/src/master/Lab%200xFF%20week%201/
#
#  @section sec_FinalProjectWeek2 Final Project Week 2
#  This is week 2 of the final project. We will be working with encoder to measure how far our
#  motor have spin or travelled.
#
#  Link: https://bitbucket.org/cedwar16/me305_labs/src/master/Lab%200xFF%20week%202/Final_Project_2_encoder.py
#
#  @section sec_FinalProjectWeek3 Final Project Week 3
#  This iss week 3 of the final project. We will be working with a driver mototr. The program will spin a small DC motor by applying 
#  pulse width modulation (PWM). 
#
#  Link: https://bitbucket.org/cedwar16/me305_labs/src/master/Lab%200xFF%20week%203/
#
#  @author Christopher Edward
#
#  @copyright License Info
#
#  @date March 21, 2021
#